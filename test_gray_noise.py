#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
np.random.seed(1234)
from sklearn.utils import shuffle
import sys
from keras.preprocessing.image import array_to_img,img_to_array, load_img
from matplotlib import pyplot as plt
import numpy as np
import time
from tqdm import tqdm
from scipy.ndimage.filters import gaussian_filter
from my_utils import *
import pickle
import sys
import pylab as pl
import matplotlib as mpl
import scipy as sp
import skimage.measure as skm
import math


#python test_noise lena_gray.gif
try:
    image_filename = str(sys.argv[1])
except IndexError:
    print("No Model or image file specified in argv")
    raise




mean = 0.
stddev= math.sqrt(400)

print("Loading image...")
img_array      = img_to_array(sp.ndimage.imread(image_filename,mode='RGB'))
if len(img_array.shape)==3:
    #color image
    img_array = rgb2gray(img_array)
if len(img_array.shape)==2:
    pass
else:
    print("Image size unknowns")
    exit(1)

img_batch = np.zeros(tuple([1,1]+[i for i in img_array.shape]))
img_batch[0] = np.copy(img_array)

print("Applying noise filter...")

#noisy_batch    = noisy_batch(np.copy(img_batch),sigma=sigma)
print("img_array = ",img_array.shape)
noisy_batch = add_gaussian_noise_batch(np.copy(img_batch),mean=mean,stddev=stddev,min_pixel_value=0,max_pixel_value=255.,verbose=True)







min_pixel_value = 0
max_pixel_value = 255.



n_cols = 3
nlines_plot = 1
cmap = "gray"
n_imgs= len(img_batch)
label = 'MSE: %2.f, SSIM: %.2f, PSNR: %.2f'

img_index = 0


img_original = img_batch[img_index]
img_noisy  = noisy_batch[img_index]

mse_noisy = mse(img_original.squeeze(), img_noisy.squeeze())

#ssim_noisy = skm.compare_ssim(img_original.squeeze(), img_noisy.squeeze(),
 #             dynamic_range=img_original.max() - img_original.min(), multichannel=True)
#psnr_noisy = skm.compare_psnr(img_original.squeeze(), img_noisy.squeeze(),
 #             dynamic_range=img_original.max() - img_original.min())
ssim_noisy = skm.compare_ssim(img_original.squeeze(), img_noisy.squeeze(),
            dynamic_range=255.)
psnr_noisy = skm.compare_psnr(img_original.squeeze(), img_noisy.squeeze(),
            dynamic_range=255. - 0)
print ("mse_noisy = ",mse_noisy)
#psnr_noisy = 0

print("img_original shape = ",img_original.shape)
print("img_noisy shape = ",img_noisy.shape)
#print("Loss = ", lossesTestData[img_index])
f, axarr = plt.subplots(nlines_plot, n_cols,figsize=(23, 10), squeeze=False)
print("image plot axis shape=",axarr.shape)
axarr[0][0].set_title('Imagem original')
axarr[0][0].imshow(img_original.squeeze(),interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)

axarr[0][1].set_title(u'Imagem com ruído')
axarr[0][1].imshow(img_noisy.squeeze(),interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)
axarr[0][1].set_xlabel(label % (mse_noisy, ssim_noisy,psnr_noisy))

axarr[0][2].set_title(u'Erro absoluto sem processamento')
im_noproc = axarr[0][2].imshow(np.absolute(img_noisy-img_original).squeeze(),interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)
#print(axarr.ravel().tolist())
cbar_original = f.colorbar(im_noproc, ax=axarr[0].ravel().tolist())


#cbar.ax.set_yticklabels([str(vmin), '0', str(vmax)])  # vertically oriented colorbar
plt.show()



