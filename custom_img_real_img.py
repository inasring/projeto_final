#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
np.random.seed(1234)
from keras.datasets import cifar10
from keras.layers import merge, Input, Dropout
from keras.layers.convolutional import Convolution2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers.pooling import MaxPooling2D,GlobalAveragePooling2D
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.regularizers import l2, activity_l2, l1
from keras.optimizers import SGD, Adam, RMSprop, Nadam
from keras.callbacks import LearningRateScheduler, ModelCheckpoint, EarlyStopping
from keras.callbacks import History
import keras.backend as K
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import sys
import csv
from itertools import izip
from dense_net import dense_net
from keras.preprocessing.image import array_to_img,img_to_array, load_img
from matplotlib import pyplot as plt
import numpy as np
import time
from tqdm import tqdm
from scipy.ndimage.filters import gaussian_filter
from my_utils import *
from keras.models import load_model
import pickle
import sys
import pylab as pl
import matplotlib as mpl
import scipy as sp
import skimage.measure as skm
import math
def mse(x, y):
    return np.linalg.norm(x - y)

try:
    model_filename = str(sys.argv[1])
    image_filename = str(sys.argv[2])
except IndexError:
    print("No Model or image file specified in argv")
    raise
sigma  = 2
print("Loading model...")
denoise_network = load_model(model_filename)
print("Loading image...")

mean = 0.
stddev= math.sqrt(400)

img_array      = img_to_array(sp.ndimage.imread(image_filename,mode='RGB'))
img_batch = np.zeros(tuple([1]+[i for i in img_array.shape]))
img_batch[0] = np.copy(img_array)
print("Applying noise filter...")
noisy_batch  = np.copy(img_batch)

##Processing block    [preprocessing]->[network]->[recovering from preprocessing]
print("Preprocessing...")
noisy_batch_preproc = pre_proc_hardcoded(np.copy(noisy_batch))
print("Processing...")
predicted_preproc = denoise_network.predict(np.copy(noisy_batch_preproc),batch_size=64)
for i in xrange(1):
    predicted_preproc = denoise_network.predict(np.copy(predicted_preproc),batch_size=64)

print("Undoing Preprocessing...")
predicted = undo_pre_proc(np.copy(predicted_preproc))

predicted   /=255.
img_batch   /=255.
noisy_batch /=255.


min_pixel_value = 0
max_pixel_value = 1.



n_cols = 3
nlines_plot = 2
cmap = "gray"
n_imgs= len(img_batch)
img_processed= np.clip(predicted[0],min_pixel_value , max_pixel_value)
plt.imsave(image_filename+str("_3")+str(".png"),img_processed.transpose(1, 2, 0))
plt.show()

