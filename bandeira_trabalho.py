from __future__ import print_function
import numpy as np
np.random.seed(1234)
from keras.layers import add
from keras.datasets import cifar10
from keras.layers import merge, Input
from keras.models import Model
from keras.optimizers import SGD, Adam
from keras.callbacks import ModelCheckpoint, EarlyStopping
from sklearn.model_selection import train_test_split
import sys
import csv
from dense_net import denseBlock_layout,bnReluConvDrop
from my_utils import pre_proc
import pickle
sys.setrecursionlimit(10000)
from unet_model import get_unet
#Usage
# python run_net database.p filename



img_rows, img_cols = 32, 32
img_channels = 3
nb_classes = 10
penalization = 0.0001

import imageio
libproblem = "library"
problem = libproblem
if problem == libproblem:
    img_1 = imageio.imread("library.png")
    mask  = imageio.imread("library_mask.png")
# if problem ==

mask  = mask[:,:,np.newaxis]
mask  = np.repeat(mask,repeats=3,axis=2)
print (mask.shape)
mask_norm = mask//255
noise_img = mask_norm*img_1
print(np.max(noise_img))
print(np.min(noise_img))
# import matplotlib.pyplot as plt
# fig = plt.figure()
# ax1  = fig.add_subplot(141)
# ax2  = fig.add_subplot(142)
# ax3  = fig.add_subplot(143)
# ax4  = fig.add_subplot(144)
# ax1.imshow(img_1)
# ax2.imshow(noise_img)
# ax3.imshow(mask)
# ax4.imshow(img_1-noise_img)
# plt.show()
# exit(1)

##Preproc
# mean = np.mean(noise_img,axis=(0,1))
# std  = np.std(noise_img,axis=(0,1))
# noise_img_preproc = (noise_img-mean)/std
noise_img_preproc = (noise_img/255 - 0.5)*2


X_train = noise_img_preproc[np.newaxis]
Y_train = np.copy(X_train)
X_test  = np.copy(X_train)
Y_test  = np.copy(X_train)



input_shape = noise_img_preproc[0].shape
droprate=0.3
batch_size = 64
nb_epoch   = 300000
data_augmentation = False
n_dense_blocks = 3

k = 12 #growth rate: number of feature maps at each convolution output inside a dense block
nb_layers = 12



feature_map_n_list = [k]*nb_layers
bnL2norm=0.0001

n_filter_initial = 16
inp = Input(shape=(None,None,Y_train.shape[-1]))
#x = Convolution2D(n_filter_initial, 3, 3, border_mode='same', init='he_normal', W_regularizer = l2(bnL2norm),bias=False,input_shape=(1, None, None))(inp)
x,n_filter = denseBlock_layout(inp,feature_map_n_list,n_filter_initial,droprate=droprate)
x = bnReluConvDrop(3, 3, 3,droprate=0,stride=(1,1),weight_decay=1e-4,bnL2norm=0.0001)(x)
# x      = merge([x,inp] , mode='sum',concat_axis=1)
x      = add([x,inp])
reconstructed = x
#model = Model(inputs=inp, outputs=reconstructed)

height,width,n_channels = Y_train.shape[1:]
n_channel_in = 32

model = get_unet(n_channel_in,n_channels,height,width)



#paper
'''def lr_schedule(epoch):
    if epoch < 150: rate = 0.1
    elif epoch < 225: rate = 0.01
    elif epoch < 300: rate = 0.001
    else: rate = 0.0001
    print (rate)
    return rate'''

model.summary()
#exit(1)
# model.compile(SGD(lr=0.1, momentum=0.9, decay=0.0, nesterov=True), loss='mean_squared_error', metrics=['accuracy'])
# model.compile(Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004), loss='mean_squared_error',
#               metrics=['mean_squared_error'])
from keras import backend as K
def custom_objective(y_true, y_pred):
    '''Just another crossentropy'''
    loss = K.mean(K.square(y_true- (y_pred*mask_norm) ))
    return loss

model.compile(Adam(lr=0.0003, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=True), loss=custom_objective, metrics=['mean_squared_error'])
#model.compile(Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=True), loss=custom_objective, metrics=['mean_squared_error'])


#lrate = LearningRateScheduler(lr_schedule)



#filename  = "unblur_cifar10"
filename = "bandeira"
filepath  = filename+".h5"
results   = filename+".results"
model_checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='auto')
early_stopping   = EarlyStopping(monitor='val_loss', patience=20, verbose=1, mode='auto')
#callbacks = [lrate,model_checkpoint,early_stopping]
#callbacks = [lrate,model_checkpoint]
callbacks = [model_checkpoint,early_stopping]

print('Not using data augmentation.')
# history=model.fit(X_train, Y_train,
#             batch_size=batch_size,
#             epochs=nb_epoch,
#             validation_data=(X_test, Y_test),
#             shuffle=True,
#             callbacks=callbacks)
imgFolder = 'imgs/'
import pathlib
pathlib.Path(imgFolder).mkdir(parents=True, exist_ok=True)
loss_list = []

save_ind = 0
X_train = np.random.uniform(0,1./30,size=Y_train.shape[:-1]+(n_channel_in,))
for i in range(nb_epoch):
    loss = model.train_on_batch(X_train,Y_train)
    print("iter:",i," loss:",loss)
    loss_list.append(loss)
    if i%10:
        X_train+=np.random.normal(0,1./300,size=Y_train.shape[:-1]+(n_channel_in,))
    if i%50 == 0:
        pred_img = model.predict(X_train)
        # pos_proc_pred_img = pred_img*std + mean
        pos_proc_pred_img = (pred_img+1)/2.
        leading_zeros = len(str(nb_epoch))
        format_str = "{:0"+str(leading_zeros)+"d}"
        imgname = "img_iter_"+format_str.format(save_ind)+".png"
        print("saving image:",imgname)
        imageio.imwrite(imgFolder+imgname, pos_proc_pred_img[0][:,:,:])
        save_ind+=1
