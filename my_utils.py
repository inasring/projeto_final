from __future__ import print_function
import numpy as np
from scipy.ndimage.filters import gaussian_filter
def blurr_all_channels(image,sigma=5):
	'''Expects and image in the format (channel,line,col)
	   Copies image to a new array that is returned with the image gaussian filtered with sigma standard deviation
	'''
	processed_image= np.copy(image)
	for channel in xrange(len(processed_image)):
		processed_image[channel] = gaussian_filter(processed_image[channel], sigma=sigma)
	return processed_image

def blurr_batch(image_batch,sigma=5,verbose=False):
	'''Applies gaussian blurr to all images in a batch
	Expects tensor in the theano format (n_image,channel,line,col)
	Copies image to a new array that is returned with the image gaussian filtered with sigma standard deviation
	'''
	#processed_image= np.copy(image)
	#for channel in xrange(len(processed_image)):
	#	processed_image[channel] = gaussian_filter(processed_image[channel], sigma=sigma)
	#return processed_image
	if verbose:
		from tqdm import tqdm
		print ("Blurring images...")
		iterator = tqdm(xrange(len(image_batch)))
	else:
		iterator = xrange(len(image_batch))
	processed_batch= np.copy(image_batch)
	for image_n in iterator:
		#processed_batch[image_n] = blurr_all_channels_in_place(image_batch[image_n],sigma=sigma)
		processed_batch[image_n] = blurr_all_channels(image_batch[image_n],sigma=sigma)
	if verbose:
		print ("Done Blurring images.")
	return processed_batch

def blurr_batch_list(image_batch,sigma_list=[1],verbose=False):
    '''Applies gaussian blurr to all images in a batch
    Expects tensor in the theano format (n_image,channel,line,col)
    Copies image to a new array that is returned with the image gaussian filtered with sigma standard deviation
    '''
    #processed_image= np.copy(image)
    #for channel in xrange(len(processed_image)):
    #   processed_image[channel] = gaussian_filter(processed_image[channel], sigma=sigma)
    #return processed_image
    if verbose:
        from tqdm import tqdm
        print ("Blurring images...")
        iterator = tqdm(xrange(len(image_batch)))
    else:
        iterator = xrange(len(image_batch))
    processed_batch= np.copy(image_batch)
    for image_n in iterator:
        #processed_batch[image_n] = blurr_all_channels_in_place(image_batch[image_n],sigma=sigma)
        processed_batch[image_n] = blurr_all_channels(image_batch[image_n],sigma=sigma_list[image_n])
    if verbose:
        print ("Done Blurring images.")
    return processed_batch

def add_gaussian_noise_batch(image_batch,mean=0,stddev=1,min_pixel_value=0.,max_pixel_value=255.,verbose=False,random_state=np.random.RandomState(1234).get_state()):
    '''Applies gaussian noise to all images in a batch
    Expects tensor in the theano format (n_image,channel,line,col)
    Copies image to a new array that is returned with gaussian noise added and clipped between min_pixel_value and max_pixel_value
    '''
    rng = np.random.RandomState(0)
    rng.set_state(random_state)
    
    n_channels=image_batch.shape[1:][0]
    if verbose:
        from tqdm import tqdm
        print ("Adding gaussian noise to images...")
        iterator = tqdm(xrange(len(image_batch)))
    else:
        iterator = xrange(len(image_batch))
    processed_batch= np.copy(image_batch)
    #print ("number of passes for iterator, should be 1 = ", len(iterator))
    for image_n in iterator:
        if verbose:
            print ("number of passes for channel, should be 1 = ", n_channels)
        for i in range(n_channels):
            #processed_batch[image_n][ i, :, :] = np.clip(image_batch[image_n][ i, :, :] +mean+stddev*np.random.normal(0,1,image_batch[image_n][i, :, :].shape),min_pixel_value,max_pixel_value)
            processed_batch[image_n][ i, :, :] = image_batch[image_n][ i, :, :] +mean+stddev*rng.normal(0,1,image_batch[image_n][i, :, :].shape)
    if verbose:
        print ("Done adding gaussian noise.")
    return processed_batch

def pre_proc(X_train,X_test,Y_train,Y_test):
    import keras.backend as K
    img_dim = X_train.shape[1:]
    if K.image_dim_ordering() == "theano":
        #Y_train/=255.
        #Y/=255.
        n_channels = img_dim[0]
        for i in range(n_channels):
            mean = np.mean(X_train[:, i, :, :]) #using train only mean and std
            std = np.std(X_train[:, i, :, :])
            X_train[:, i, :, :] = (X_train[:, i, :, :] - mean) / std
            X_test[:, i, :, :] = (X_test[:, i, :, :] - mean) / std
            Y_train[:, i, :, :] = (Y_train[:, i, :, :] - mean) / std
            Y_test[:, i, :, :] = (Y_test[:, i, :, :] - mean) / std
            print ("channel_mean  = ",mean)
            print ("channel_std  = ",std)
    elif K.image_dim_ordering() == "tensorflow":
        n_channels = img_dim[-1]
        for i in range(n_channels):
            mean = np.mean(X_train[:, :, :, i]) #using train only mean and std
            std = np.std(X_train[:, :, :, i])
            X_train[:, :, :, i] = (X_train[:, :, :, i] - mean) / std
            X_test[:, :, :, i] = (X_test[:, :, :, i] - mean) / std
            Y_train[:, i, :, :] = (Y_train[:, i, :, :] - mean) / std
            Y_test[:, i, :, :]  =(Y_test[:, i, :, :] - mean) / std
    return (X_train,X_test,Y_train,Y_test)

def pre_proc_mean_std(X_train,X_test,Y_train,Y_test,mean_list,std_list):
    import keras.backend as K
    img_dim = X_train.shape[1:]
    if K.image_dim_ordering() == "th":
        #Y_train/=255.
        #Y/=255.
        n_channels = img_dim[0]
        for i in range(n_channels):
            X_train[:, i, :, :] = (X_train[:, i, :, :] - mean_list[i]) / std_list[i]
            X_test[:, i, :, :] = (X_test[:, i, :, :] - mean_list[i]) / std_list[i]
            Y_train[:, i, :, :] = (Y_train[:, i, :, :] - mean_list[i]) / std_list[i]
            Y_test[:, i, :, :] = (Y_test[:, i, :, :] - mean_list[i]) / std_list[i]
    elif K.image_dim_ordering() == "tf":
        n_channels = img_dim[-1]
        for i in range(n_channels):
            X_train[:, :, :, i] = (X_train[:, :, :, i] - mean_list[i]) / std_list[i]
            X_test[:, :, :, i] = (X_test[:, :, :, i] - mean_list[i]) / std_list[i]
            Y_train[:, i, :, :] = (Y_train[:, i, :, :] - mean_list[i]) / std_list[i]
            Y_test[:, i, :, :]  =(Y_test[:, i, :, :] - mean_list[i]) / std_list[i]
    return (X_train,X_test,Y_train,Y_test)

def pre_proc_mean_std_single(X_train,mean_list,std_list):
    import keras.backend as K
    img_dim = X_train.shape[1:]
    if K.image_dim_ordering() == "th":
        #Y_train/=255.
        #Y/=255.
        n_channels = img_dim[0]
        for i in range(n_channels):
            X_train[:, i, :, :] = (X_train[:, i, :, :] - mean_list[i]) / std_list[i]
    elif K.image_dim_ordering() == "tf":
        n_channels = img_dim[-1]
        for i in range(n_channels):
            X_train[:, :, :, i] = (X_train[:, :, :, i] - mean_list[i]) / std_list[i]
    return X_train
def undo_pre_proc(input_image):
        img_dim = input_image.shape[1:]
        n_channels = img_dim[0]
        #Hardcoded means and std for cifar10 with train test split random seed=10
        channel_means = np.asarray([124.26855,121.92995,112.84782])
        channel_stds  = np.asarray([57.7062,56.947437,62.030624])
        for i in range(n_channels):
            input_image[:, i, :, :] = input_image[:, i, :, :]*channel_stds[i] + channel_means[i]
        return input_image
def undo_pre_proc_gray_hardcoded(input_image):
        img_dim = input_image.shape[1:]
        n_channels = img_dim[0]
        #Hardcoded means and std for cifar10 with train test split random seed=10
        channel_means = np.asarray([121.812])
        channel_stds  = np.asarray([64.0967])
        for i in range(n_channels):
            input_image[:, i, :, :] = input_image[:, i, :, :]*channel_stds[i] + channel_means[i]
        return input_image  

def undo_pre_proc_gray_mean_std(input_image,mean_list,std_list):
        img_dim = input_image.shape[1:]
        n_channels = img_dim[0]
        #Hardcoded means and std for cifar10 with train test split random seed=10
        channel_means = np.asarray(mean_list)
        channel_stds  = np.asarray(std_list)
        for i in range(n_channels):
            input_image[:, i, :, :] = input_image[:, i, :, :]*channel_stds[i] + channel_means[i]
        return input_image  

def pre_proc_hardcoded(input_image):
        '''Removes hardcoded cifar10 average and standard deviation from a single image in format n,channel,rows,col image'''
        n_channels = input_image.shape[1:][0]
        #Hardcoded means and std for cifar10 with train test split random seed=10
        channel_means = np.asarray([124.26855,121.92995,112.84782])
        channel_stds  = np.asarray([57.7062,56.947437,62.030624])
        for i in range(n_channels):
            input_image[:, i, :, :] = (input_image[:, i, :, :]-channel_means[i])/channel_stds[i]
        return input_image
        
def pre_proc_hardcoded_gray(input_image):
        '''Removes hardcoded cifar10 average and standard deviation from a single image in format n,channel,rows,col image'''
        n_channels = input_image.shape[1:][0]
        #Hardcoded means and std for cifar10 with train test split random seed=10
        channel_means = np.asarray([121.812])
        channel_stds  = np.asarray([64.0967])
        for i in range(n_channels):
            input_image[:, i, :, :] = (input_image[:, i, :, :]-channel_means[i])/channel_stds[i]
        return input_image


def rgb2gray(rgb):

    r, g, b = rgb[1,:,:], rgb[1,:,:], rgb[2,:,:]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    #gray = 0.2125 * r + 0.7154 * g + 0.0721  * b
    return gray


def rgb2gray_batch(image_batch,verbose=False):
    processed_batch = np.zeros(tuple([len(image_batch)] + [1] +  [i for i in image_batch.shape[2:]]))
    print("Processing batch shape = ",processed_batch.shape)
    n_channels=processed_batch.shape[1:][0]
    if verbose:
        from tqdm import tqdm
        print("Turning images to grayscale...")
        iterator = tqdm(xrange(len(image_batch)))
    else:
        iterator = xrange(len(image_batch))
    for image_n in iterator:
        rgb = rgb2gray(image_batch[image_n])
        processed_batch[image_n] = rgb [:,:]
    if verbose:
        print("Done processing.")
    return processed_batch

def interleave(array_1,array_2):
    #Interleaves two multidimensional arrays by their first dimension
    assert(array_1.shape[0]==array_2.shape[0])
    interleaved = np.zeros(tuple([array_1.shape[0]*2]+[i for i in array_1.shape[1::]]))
    interleaved [::2] = array_1
    interleaved [1::2] = array_2
    return interleaved

def pre_proc_gan(input_image):
        '''Removes hardcoded cifar10 average and standard deviation from a single image in format n,channel,rows,col image'''
        input_image-=(255./2)
        input_image/=(255./2)
        return input_image

def undo_pre_proc_gan(input_image):
        '''Removes hardcoded cifar10 average and standard deviation from a single image in format n,channel,rows,col image'''
        input_image*=(255./2)
        input_image+=(255./2)
        return input_image

def mse(x, y):
    return ((x - y) ** 2).mean(axis=None)

def minibatch_indexes(siz_x,batch_size):
    start_slice = 0
    last_iter_size = siz_x%batch_size
    corrected  = siz_x - last_iter_size
    range_start  = range(0,corrected+batch_size,batch_size)
    range_end  = range(batch_size,corrected+batch_size,batch_size)
    n_iter       = siz_x/batch_size+(last_iter_size>0)
    if corrected!=siz_x:
        range_start += [batch_size*(n_iter-1)]
        range_end += [siz_x]
    return zip(range_start,range_end)