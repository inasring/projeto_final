#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
np.random.seed(1234)
from keras.datasets import cifar10
from keras.layers import merge, Input, Dropout
from keras.layers.convolutional import Convolution2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers.pooling import MaxPooling2D,GlobalAveragePooling2D
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.regularizers import l2, activity_l2, l1
from keras.optimizers import SGD, Adam, RMSprop, Nadam
from keras.callbacks import LearningRateScheduler, ModelCheckpoint, EarlyStopping
from keras.callbacks import History
import keras.backend as K
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import sys
import csv
from itertools import izip
from dense_net import dense_net
from keras.preprocessing.image import array_to_img,img_to_array, load_img
from matplotlib import pyplot as plt
import numpy as np
import time
from tqdm import tqdm
from scipy.ndimage.filters import gaussian_filter
from my_utils import *
from keras.models import load_model
import pickle
import sys
import pylab as pl
import matplotlib as mpl
import scipy as sp
import skimage.measure as skm
import math
from mpl_toolkits.axes_grid1 import make_axes_locatable, axes_size

#python denoise_gray_cifar10.h5 lena_gray.gif
try:
    model_filename = str(sys.argv[1])
    image_filename = str(sys.argv[2])
    folder_name    = str(sys.argv[3])
except IndexError:
    print("No Model or image file specified in argv")
    raise
sigma  = 2.5
print("Loading model...")
denoise_network = load_model(model_filename)


mean = 0.
stddev= math.sqrt(400)

print("Loading image "+image_filename +" ...")
img_array      = img_to_array(sp.ndimage.imread(image_filename,mode='RGB'))
if len(img_array.shape)==3:
    #color image
    img_array = rgb2gray(img_array)
if len(img_array.shape)==2:
    pass
else:
    print("Image size unknown ")
    exit(1)

img_batch = np.zeros(tuple([1,1]+[i for i in img_array.shape]))
img_batch[0] = np.copy(img_array)

print("Applying noise filter...")

#noisy_batch    = noisy_batch(np.copy(img_batch),sigma=sigma)
print("img_array = ",img_array.shape)
rng = np.random.RandomState(1234)
noisy_batch = add_gaussian_noise_batch(np.copy(img_batch),mean=mean,stddev=stddev,min_pixel_value=0,max_pixel_value=255.,verbose=True,random_state=rng.get_state())
##Processing block    [preprocessing]->[network]->[recovering from preprocessing]

from timeit import default_timer as timer
start = timer()

print("Preprocessing...")
noisy_batch_preproc = pre_proc_hardcoded_gray(np.copy(noisy_batch))
#noisy_batch_preproc = pre_proc_gan(np.copy(noisy_batch))
print("Processing...")
predicted_preproc = denoise_network.predict(np.copy(noisy_batch_preproc),batch_size=64)
for i in xrange(0):
    predicted_preproc = denoise_network.predict(np.copy(predicted_preproc),batch_size=64)

print("Undoing Preprocessing...")
predicted = undo_pre_proc_gray(np.copy(predicted_preproc))

end = timer()
print("Processing time elapsed:", (end - start))




#predicted   /=255.
#img_batch   /=255.
#noisy_batch /=255.


min_pixel_value = 0
max_pixel_value = 255.



n_cols = 3
nlines_plot = 1
cmap = "gray"
n_imgs= len(img_batch)
label = 'SSIM: %.2f , RMSE: %2.2f'

img_index = 0
img_original = np.clip(img_batch[img_index],min_pixel_value , max_pixel_value)
img_noisy  = np.clip(noisy_batch[img_index],min_pixel_value , max_pixel_value)
img_processed= np.clip(predicted[img_index],min_pixel_value , max_pixel_value)

# img_original = img_batch[img_index]
# img_noisy  = noisy_batch[img_index]
# img_processed= predicted[img_index]


mse_noisy = mse(img_original.squeeze(), img_noisy.squeeze())
ssim_noisy = skm.compare_ssim(img_original.squeeze(), img_noisy.squeeze(),
              dynamic_range=255.)
psnr_noisy = skm.compare_psnr(img_original.squeeze(), img_noisy.squeeze(),
              dynamic_range=255.)

mse_denoised = mse(img_original.squeeze(), img_processed.squeeze())
ssim_denoised = skm.compare_ssim(img_original.squeeze().astype('float32'), img_processed.squeeze().astype('float32'),
              dynamic_range=255.)
psnr_denoised = skm.compare_psnr(img_original.squeeze().astype('float32'), img_processed.squeeze().astype('float32'),
              dynamic_range=255.)


rmse_denoised = np.sqrt(mse_denoised)


print("img_original shape = ",img_original.shape)
print("img_noisy shape = ",img_noisy.shape)
print("img_processed shape = ",img_processed.shape)
#print("Loss = ", lossesTestData[img_index])
f, axarr = plt.subplots(nlines_plot, n_cols,figsize=(23, 10))
print("image plot axis shape=",axarr.shape)

axarr[0].set_title(u'Imagem com ruído')
axarr[0].imshow(img_noisy.squeeze(),interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)

axarr[1].set_title(u'Imagem processada')
axarr[1].imshow(img_processed.squeeze(),interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)
#axarr[1].set_xlabel(label % (ssim_denoised, rmse_denoised))

axarr[2].set_title(u'Erro absoluto')
error_img = np.absolute(img_processed-img_original).squeeze()
print ("Maximum error = ", np.amax(error_img))
im_proc = axarr[2].imshow(error_img,interpolation='none',cmap='gray_r',vmin=min_pixel_value, vmax=60)
#print(axarr.ravel().tolist())

for axis in axarr:
  axis.get_yaxis().set_visible(False)
  axis.get_xaxis().set_visible(False)




#cbar_processed = f.colorbar(im_proc,fraction=0.046, pad=0.04)


# divider = make_axes_locatable(axarr[2])
# ax_cb = divider.new_horizontal(size="5%", pad=0.05)
# fig1 = axarr[2].get_figure()
# fig1.add_axes(ax_cb)

#Z, extent = get_demo_image()
#im = ax.imshow(Z, extent=extent, interpolation="nearest")

# plt.colorbar(im_proc, cax=ax_cb)
# ax_cb.yaxis.tick_right()
# for tl in ax_cb.get_yticklabels():
#     tl.set_visible(False)
# ax_cb.yaxis.tick_right()
# create color bar

box = axarr[2].get_position()
axColor = plt.axes([box.x0*1.05 + box.width * 1.05, box.y0, 0.01, box.height])
plt.colorbar(im_proc, cax = axColor, orientation="vertical")


# divider = make_axes_locatable(axarr[2])
# cax1 = divider.append_axes("right", size="5%", pad=0.05)
# cbar = f.colorbar(im_proc, cax = cax1)





def remove_last(string,separator):
  return separator.join(string.split(separator)[:-1])

name = image_filename.split("/")[-1]
name = remove_last(name,".")
np.savez(folder_name+"/"+name+".npz", img_original=img_original.squeeze(),img_noisy=img_noisy.squeeze() ,img_processed=img_processed.squeeze(),error_img=error_img)

f.savefig(folder_name+"/"+name ,bbox_inches='tight')
#fig2.savefig("lena_tests/lena_modelD_bicubicinterp_"+str(dpi)+".png",bbox_inches='tight', dpi = dpi)
#plt.show()



