from __future__ import print_function
import numpy as np
np.random.seed(1234)
from keras.datasets import cifar10
import numpy as np
from tqdm import tqdm
from scipy.ndimage.filters import gaussian_filter
from my_utils import *

random_seed_number =0 
np.random.seed(random_seed_number)

(X_train, y_train), (X_test, y_test) = cifar10.load_data()
print(X_train.shape)
sample = X_train[0]
print(sample.shape)

max_sigma = 4
min_sigma = 1
sigma_list = np.random.uniform(min_sigma,max_sigma,len(X_train))
print("Using sigma_list= ",sigma_list)
blurred_batch = blurr_batch_list(X_train, sigma_list=sigma_list,verbose=True)
import pickle
filename = "blurred_cifar10_random_sigma_from_"+str(max_sigma)+"_to_"+str(min_sigma)+"sigma"+"randSeed_"+str(random_seed_number)+".p"
print("Dumping picke file...")
pickle.dump( blurred_batch, open( filename, "wb" ))
print("dumped pickle file to ",filename)



if __name__ == "__main__":
    ################################
    import pylab as pl
    from matplotlib import pyplot as plt
    import time
    from sklearn.model_selection import train_test_split


    blurred_batch = pickle.load(open( filename, "rb" ))
    (original_x, _), (_, _) = cifar10.load_data()
    X_train, X_test, Y_train, Y_test = train_test_split(blurred_batch,original_x, test_size=0.1,random_state=10)

    beg_range=10
    end_range=25
    n_evals = end_range - beg_range
    #(X_train,X_test,Y_train,Y_test) = pre_proc(X_train,X_test,Y_train,Y_test)
    print(X_test.shape)
    X_test = X_test[beg_range:end_range]
    Y_test = Y_test[beg_range:end_range]
    for img_index in tqdm(xrange(n_evals)):
        img_blurred = X_test[img_index]
        img_original= Y_test[img_index]
        f = pl.figure()
        f.add_subplot(1 , 3 , 1)
        plt.imshow(img_original.transpose(1, 2, 0),interpolation='none')
        f.add_subplot(1 , 3 , 3)
        plt.imshow(img_blurred.transpose(1, 2, 0),interpolation='none')
        print("Blur used = ", sigma_list[img_index])
        plt.show()
        time.sleep(0.2)
    #################################   