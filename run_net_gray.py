from __future__ import print_function
import numpy as np
np.random.seed(1234)

from keras.datasets import cifar10
from keras.layers import merge, Input
from keras.models import Model
from keras.optimizers import SGD, Nadam
from keras.callbacks import ModelCheckpoint, EarlyStopping
from sklearn.model_selection import train_test_split
import sys
import csv
from dense_net import bnReluConvDropBias,denseBlock_layout,bnConv
import hickle
from my_utils import pre_proc
sys.setrecursionlimit(10000)
#Usage
# python run_net_gray.py gray_gaussian_noise_cifar10_mean_0.0_stddev_20.0randSeed_0.hickle gray_cifar10.hickle
try:
    noisy_dataset_filename = str(sys.argv[1])
    target_dataset_filename = str(sys.argv[2])
except IndexError:
    print("Datasets not specified in argv")
    raise



img_rows, img_cols = 32, 32
img_channels = 3
nb_classes = 10
penalization = 0.0001

# the data, shuffled and split between train and test sets
noisy_batch = hickle.load(open( str(noisy_dataset_filename), "r" ))
original_images = hickle.load(open( str(target_dataset_filename), "r" ))
#Blurred images are X_train and X_test
#Original images are Y_train and Y_test
X_train, X_test, Y_train, Y_test = train_test_split(noisy_batch,original_images, test_size=0.1,random_state=10)


X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
Y_train = Y_train.astype('float32')
Y_test = Y_test.astype('float32')


X_train, X_test, Y_train, Y_test = pre_proc(X_train,X_test,Y_train,Y_test)

# X_train  = pre_proc_gan(X_train)
# X_test   = pre_proc_gan(X_test)
# Y_train  = pre_proc_gan(Y_train)
# Y_test   = pre_proc_gan(Y_test)





print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')



input_shape = X_train[0].shape
droprate=0.5
batch_size = 64
nb_epoch = 30
data_augmentation = False
n_dense_blocks = 3

# k = 1 #growth rate: number of feature maps at each convolution output inside a dense block
k = 12 #MSE = 0.02734
k = 4 #MSE = 0.03246  nb_layers=15
k = 10 #MSE = 0,3243  SSIM = 0,83
k = 12 #MSE =   SSIM =
k = 8
nb_layers = 8


feature_map_n_list = [k]*nb_layers
bnL2norm=0.0001

n_filter_initial = 0
inp = Input(shape=(1,None,None))
x = bnReluConvDropBias(16, 3, 3,droprate=0.,stride=(1,1),weight_decay=1e-4,bnL2norm=0.0001,bias=False)(inp)
x = bnReluConvDropBias(16, 3, 3,droprate=0.,stride=(1,1),weight_decay=1e-4,bnL2norm=0.0001,bias=False)(x)
x,n_filter = denseBlock_layout(x,feature_map_n_list,n_filter_initial,droprate=droprate)
#x      = merge([x,inp] , mode="concat",concat_axis=1)

x = bnConv(1, 1, 1,stride=(1,1),weight_decay=1e-4,bnL2norm=0.0001,bias=True)(x)
#x      = Activation('tanh')(x)
reconstructed = x
model = Model(input=inp, output=reconstructed)




#paper
'''def lr_schedule(epoch):
    if epoch < 150: rate = 0.1
    elif epoch < 225: rate = 0.01
    elif epoch < 300: rate = 0.001
    else: rate = 0.0001
    print (rate)
    return rate'''

model.summary()
#exit(1)
#model.compile(SGD(lr=0.1, momentum=0.9, decay=0.0, nesterov=True), loss='mean_squared_error', metrics=['accuracy'])
#model.compile(Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004), loss='mean_absolute_error',
 #             metrics=['mean_absolute_error'])
model.compile(Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004), loss='mean_squared_error',
              metrics=['mean_squared_error'])



#lrate = LearningRateScheduler(lr_schedule)


from datetime import datetime
string_date = datetime.now().strftime('%Y-%m-%d.%H:%M:%S')
#filename  = "gray_gaussian_noise"+"simple_net"
filename  = "gray_gaussian_noise"+"_k"+str(k)+"L"+str(nb_layers)+"_dropout_"+str(droprate)+"_"
filename  = filename+"_"+string_date
filepath  = filename+".h5"
results   = filename+".results"
model_checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='auto')
early_stopping   = EarlyStopping(monitor='val_loss', patience=20, verbose=1, mode='auto')
#callbacks = [lrate,model_checkpoint,early_stopping]
#callbacks = [lrate,model_checkpoint]
callbacks = [model_checkpoint,early_stopping]

print('Not using data augmentation.')
history=model.fit(X_train, Y_train,
            batch_size=batch_size,
            epochs=nb_epoch,
            validation_data=(X_test, Y_test),
            shuffle=True,
            callbacks=callbacks)
f= open(results, 'wb')
writer = csv.writer(f)
writer.writerows(zip(history.history['loss'],history.history['val_loss']))
f.close()
