#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
np.random.seed(1234)

from keras.datasets import cifar10
from keras.layers import merge, Input, Dropout
from keras.layers.convolutional import Convolution2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers.pooling import MaxPooling2D,GlobalAveragePooling2D
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.regularizers import l2, activity_l2, l1
from keras.optimizers import SGD, Adam, RMSprop, Nadam
from keras.callbacks import LearningRateScheduler, ModelCheckpoint, EarlyStopping
from keras.callbacks import History
import keras.backend as K
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import sys
import csv
from itertools import izip
from dense_net import dense_net
from keras.preprocessing.image import array_to_img,img_to_array, load_img
from matplotlib import pyplot as plt
import numpy as np
import time
from tqdm import tqdm
from scipy.ndimage.filters import gaussian_filter
from my_utils import *
from keras.models import load_model
import pickle
import sys
import pylab as pl
import matplotlib as mpl

try:
	model_filename = str(sys.argv[1])
except IndexError:
	print("No Model file specified in argv")
	raise
deblur_network = load_model(model_filename)



beg_range=80
end_range=120
n_evals = end_range - beg_range

print ("Loading Model...")
blurred_batch = pickle.load(open( "blurred_cifar10.p", "rb" ))
(original_x, _), (_, _) = cifar10.load_data()
X_train, X_test, Y_train, Y_test = train_test_split(blurred_batch,original_x, test_size=0.1,random_state=10)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
Y_train = Y_train.astype('float32')
Y_test = Y_test.astype('float32')


X_train_predict, X_test_predict, Y_train, Y_test = pre_proc(X_train,X_test,Y_train,Y_test)

X_test          = X_test[beg_range:end_range]
X_test_predict  = X_test_predict[beg_range:end_range]
Y_test          = Y_test[beg_range:end_range]


max_sigma = 4
min_sigma = 1
random_seed_number = 0
np.random.seed(random_seed_number)
sigma_list = np.random.uniform(min_sigma,max_sigma,len(X_train))
print ("Processing input...")
processedTestData = deblur_network.predict(X_test_predict,batch_size=64)
#processedTestData = deblur_network.predict(processedTestData,batch_size=64)
#processedTestData = deblur_network.predict(processedTestData,batch_size=64)

lossesTestData    = deblur_network.evaluate(X_test_predict,Y_test,batch_size=64)


#Undo preprocessing before showing
Y_test_undone_preproc                = undo_pre_proc(Y_test)
processedTestData_undone_preproc     = undo_pre_proc(processedTestData)
X_test_undone_preproc                = undo_pre_proc(X_test)


Y_test_undone_preproc /=255.
processedTestData_undone_preproc /=255.
X_test_undone_preproc /=255.

print(X_test.shape)

min_pixel_value = 0
max_pixel_value = 1.

n_subplots = 4
for img_index in tqdm(xrange(n_evals)):
    img_original = Y_test[img_index]
    img_must_be_original = Y_test_undone_preproc[img_index]
    img_blurred  = X_test[img_index]
    img_processed= processedTestData_undone_preproc[img_index]
    #print("Loss = ", lossesTestData[img_index])
    f, axarr = plt.subplots(1, n_subplots,figsize=(14, 5))

    axarr[0].set_title('Imagem original')
    axarr[0].imshow(np.clip(img_original.transpose(1, 2, 0),min_pixel_value , max_pixel_value),interpolation='none')

    axarr[1].set_title('Imagem com blur')
    axarr[1].imshow(np.clip(img_blurred.transpose(1, 2, 0),min_pixel_value , max_pixel_value),interpolation='none')

    axarr[2].set_title('Imagem deblurred')
    axarr[2].imshow(np.clip(img_processed.transpose(1, 2, 0),min_pixel_value , max_pixel_value),interpolation='none')
    print("Blur used = ", sigma_list[img_index])

    axarr[3].set_title(u'Erro absoluto')

    cmap = "gray"
   #im = axarr[3].imshow(rgb2gray((2*(img_processed-img_original))/(img_original+img_processed)),vmin=vmin, vmax=vmax,interpolation='none',cmap=cmap)
    im = axarr[3].imshow(np.absolute(rgb2gray(img_processed-img_original)),interpolation='none',cmap=cmap)
    cbar = f.colorbar(im, ax=axarr.ravel().tolist())
    #cbar.ax.set_yticklabels([str(vmin), '0', str(vmax)])  # vertically oriented colorbar
    plt.show()
    time.sleep(0.2)

