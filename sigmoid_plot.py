import scipy as sp
import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-6,6,300)
y = 1./(1+np.exp(-x))

#plt.scatter(x,y)
plt.plot(x,y)
plt.grid()
plt.show()