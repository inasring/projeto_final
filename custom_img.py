#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
np.random.seed(1234)
from keras.datasets import cifar10
from keras.layers import merge, Input, Dropout
from keras.layers.convolutional import Convolution2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers.pooling import MaxPooling2D,GlobalAveragePooling2D
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.regularizers import l2, activity_l2, l1
from keras.optimizers import SGD, Adam, RMSprop, Nadam
from keras.callbacks import LearningRateScheduler, ModelCheckpoint, EarlyStopping
from keras.callbacks import History
import keras.backend as K
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import sys
import csv
from itertools import izip
from dense_net import dense_net
from keras.preprocessing.image import array_to_img,img_to_array, load_img
from matplotlib import pyplot as plt
import numpy as np
import time
from tqdm import tqdm
from scipy.ndimage.filters import gaussian_filter
from my_utils import *
from keras.models import load_model
import pickle
import sys
import pylab as pl
import matplotlib as mpl
import scipy as sp
import skimage.measure as skm
import math

try:
    model_filename = str(sys.argv[1])
    image_filename = str(sys.argv[2])
except IndexError:
    print("No Model or image file specified in argv")
    raise
sigma  = 2.5
print("Loading model...")
deblur_network = load_model(model_filename)
print("Loading image...")

mean = 0.
stddev= math.sqrt(50*50)

img_array      = img_to_array(sp.ndimage.imread(image_filename,mode='RGB'))
img_batch = np.zeros(tuple([1]+[i for i in img_array.shape]))
img_batch[0] = np.copy(img_array)
print("Applying noise filter...")
#noisy_batch    = blurr_batch(np.copy(img_batch),sigma=sigma)
noisy_batch = add_gaussian_noise_batch(np.copy(img_batch),mean=mean,stddev=stddev,min_pixel_value=0,max_pixel_value=255.)

##Processing block    [preprocessing]->[network]->[recovering from preprocessing]
print("Preprocessing...")
noisy_batch_preproc = pre_proc_hardcoded(np.copy(noisy_batch))
print("Processing...")
predicted_preproc = deblur_network.predict(np.copy(noisy_batch_preproc),batch_size=64)
predicted_preproc = deblur_network.predict(np.copy(predicted_preproc),batch_size=64)
predicted_preproc = deblur_network.predict(np.copy(predicted_preproc),batch_size=64)
predicted_preproc = deblur_network.predict(np.copy(predicted_preproc),batch_size=64)
predicted_preproc = deblur_network.predict(np.copy(predicted_preproc),batch_size=64)

print("Undoing Preprocessing...")
predicted = undo_pre_proc(np.copy(predicted_preproc))

predicted   /=255.
img_batch   /=255.
noisy_batch /=255.


min_pixel_value = 0
max_pixel_value = 1.



n_cols = 3
nlines_plot = 2
cmap = "gray"
n_imgs= len(img_batch)
label = 'MSE: %2.f, SSIM: %.2f, PSNR: %.2f'
for img_index in tqdm(xrange(n_imgs)):
    img_original = np.clip(img_batch[img_index],min_pixel_value , max_pixel_value)
    img_blurred  = np.clip(noisy_batch[img_index],min_pixel_value , max_pixel_value)
    img_processed= np.clip(predicted[img_index],min_pixel_value , max_pixel_value)

    mse_blur = mse(img_original.transpose(1, 2, 0), img_blurred.transpose(1, 2, 0))
    ssim_blur = skm.compare_ssim(img_original.transpose(1, 2, 0), img_blurred.transpose(1, 2, 0),
                  dynamic_range=img_original.max() - img_original.min(), multichannel=True)
    psnr_blur = skm.compare_psnr(img_original.transpose(1, 2, 0), img_blurred.transpose(1, 2, 0),
                  dynamic_range=img_original.max() - img_original.min())

    mse_deblurred = mse(img_original.transpose(1, 2, 0), img_processed.transpose(1, 2, 0))
    ssim_deblurred = skm.compare_ssim(img_original.transpose(1, 2, 0).astype('float32'), img_processed.transpose(1, 2, 0).astype('float32'),
                  dynamic_range=img_original.max() - img_original.min(), multichannel=True)
    psnr_deblurred = skm.compare_psnr(img_original.transpose(1, 2, 0).astype('float32'), img_processed.transpose(1, 2, 0).astype('float32'),
                  dynamic_range=img_original.max() - img_original.min())


    print("img_original shape = ",img_original.shape)
    print("img_blurred shape = ",img_blurred.shape)
    print("img_processed shape = ",img_processed.shape)
    #print("Loss = ", lossesTestData[img_index])
    f, axarr = plt.subplots(nlines_plot, n_cols,figsize=(23, 10))
    print("image plot axis shape=",axarr.shape)
    axarr[0][0].set_title('Imagem original')
    axarr[0][0].imshow(img_original.transpose(1, 2, 0),interpolation='none')

    axarr[0][1].set_title(u'Imagem com ruído')
    axarr[0][1].imshow(img_blurred.transpose(1, 2, 0),interpolation='none')
    axarr[0][1].set_xlabel(label % (mse_blur, ssim_blur,psnr_blur))
    
    axarr[0][2].set_title(u'Erro absoluto sem processamento')
    im_noproc = axarr[0][2].imshow(np.absolute(rgb2gray(img_blurred-img_original)),interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)
    #print(axarr.ravel().tolist())
    cbar_original = f.colorbar(im_noproc, ax=axarr[0].ravel().tolist())
    
    axarr[1][0].set_title('Imagem original')
    axarr[1][0].imshow(img_original.transpose(1, 2, 0),interpolation='none')
    
    axarr[1][1].set_title('Imagem processada')
    axarr[1][1].imshow(img_processed.transpose(1, 2, 0),interpolation='none')
    axarr[1][1].set_xlabel(label % (mse_deblurred, ssim_deblurred,psnr_deblurred))
   
    axarr[1][2].set_title(u'Erro absoluto com processamento')
    im_proc = axarr[1][2].imshow(np.absolute(rgb2gray(img_processed-img_original)),interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)
    #print(axarr.ravel().tolist())
    cbar_processed = f.colorbar(im_proc, ax=axarr[1].ravel().tolist())
    
    
    #cbar.ax.set_yticklabels([str(vmin), '0', str(vmax)])  # vertically oriented colorbar
    plt.show()
    time.sleep(0.2)


