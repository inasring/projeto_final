#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
np.random.seed(1234)
from keras.datasets import cifar10
from keras.layers import merge, Input, Dropout
from keras.layers.convolutional import Convolution2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers.pooling import MaxPooling2D,GlobalAveragePooling2D
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.regularizers import l2, activity_l2, l1
from keras.optimizers import SGD, Adam, RMSprop, Nadam
from keras.callbacks import LearningRateScheduler, ModelCheckpoint, EarlyStopping
from keras.callbacks import History
import keras.backend as K
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import sys
import csv
from itertools import izip
from dense_net import dense_net
from keras.preprocessing.image import array_to_img,img_to_array, load_img
from matplotlib import pyplot as plt
import numpy as np
import time
from tqdm import tqdm
from scipy.ndimage.filters import gaussian_filter
from my_utils import *
from keras.models import load_model
import pickle
import sys
import pylab as pl
import matplotlib as mpl
import scipy as sp
import skimage.measure as skm
from timeit import default_timer as timer
import math
import pandas

#python denoise_gray_cifar10.h5 lena_gray.gif
try:
    model_filename = str(sys.argv[1])
except IndexError:
    print("No Model file specified in argv")
    raise

model_filename_no_extension = '.'.join(model_filename.split('.')[:-1])
print("Loading model "+model_filename+"...")
denoise_network = load_model(model_filename)


mean   = 0.
#stddev = math.sqrt(400)
stddevs = [math.sqrt(200),math.sqrt(400)]

imagesFolderName = "images/"
lenaFile    = "lena_gray_256x256.gif"
barbaraFile = "barbara_gray_256x256.png"
boatFile    = "boat_gray_256x256.png"
imagesFilenames = [lenaFile,barbaraFile,boatFile]
imagesFilenames = [imagesFolderName+i for i in imagesFilenames]


#dat = {'lena':{'SSIM':0.8,'MSE':40},'barbara':{'SSIM':0.79,'MSE':39}}
data_list_dict = []
data_dict      = {}

for stddev in stddevs:
    print("stddev = ",stddev)
    for image_filename in imagesFilenames:
        print("Image:",image_filename)

        img_array      = img_to_array(sp.ndimage.imread(image_filename,mode='RGB'))
        if len(img_array.shape)==3:
            #color image
            img_array = rgb2gray(img_array)
        if len(img_array.shape)==2:
            pass
        else:
            print("Image size unknowns")
            exit(1)

        img_batch = np.zeros(tuple([1,1]+[i for i in img_array.shape]))
        img_batch[0] = np.copy(img_array)
        noisy_batch = add_gaussian_noise_batch(np.copy(img_batch),mean=mean,stddev=stddev,min_pixel_value=0,max_pixel_value=255.,verbose=False)
        ##Processing block    [preprocessing]->[network]->[recovering from preprocessing]

        start = timer()

        noisy_batch_preproc = pre_proc_hardcoded_gray(np.copy(noisy_batch))
        #noisy_batch_preproc = pre_proc_gan(np.copy(noisy_batch))
        predicted_preproc = denoise_network.predict(np.copy(noisy_batch_preproc),batch_size=64)
        for i in xrange(0):
            predicted_preproc = denoise_network.predict(np.copy(predicted_preproc),batch_size=64)
        predicted = undo_pre_proc_gray(np.copy(predicted_preproc))

        end = timer()
        print("Processing time elapsed:", (end - start))

        min_pixel_value = 0
        max_pixel_value = 255.

        img_index = 0
        img_original = np.clip(img_batch[img_index],min_pixel_value , max_pixel_value)
        img_noisy  = np.clip(noisy_batch[img_index],min_pixel_value , max_pixel_value)
        img_processed= np.clip(predicted[img_index],min_pixel_value , max_pixel_value)


        mse_noisy =  skm.compare_mse(img_original.squeeze(), img_noisy.squeeze())
        rmse_noisy = math.sqrt(mse_noisy)
        ssim_noisy = skm.compare_ssim(img_original.squeeze(), img_noisy.squeeze(),
                      dynamic_range=255.)
        psnr_noisy = skm.compare_psnr(img_original.squeeze(), img_noisy.squeeze(),
                      dynamic_range=255.)

        mse_denoised  = skm.compare_mse(img_original.squeeze().astype('float32'), img_processed.squeeze().astype('float32'))
        rmse_denoised = math.sqrt(mse_denoised)
        ssim_denoised = skm.compare_ssim(img_original.squeeze().astype('float32'), img_processed.squeeze().astype('float32'),
                      dynamic_range=255.)
        psnr_denoised = skm.compare_psnr(img_original.squeeze().astype('float32'), img_processed.squeeze().astype('float32'),
                      dynamic_range=255.)

        #data_dict.update({image_filename:{'RMSE':[rmse_noisy,rmse_denoised],'SSIM':[ssim_noisy,ssim_denoised]}})
        data_dict.update({image_filename:{'RMSE':rmse_denoised,'SSIM':ssim_denoised}})

    data_list_dict.append(pandas.DataFrame.from_dict(data_dict))
    data_dict = {} 

panda_dat = pandas.concat(data_list_dict)
print(panda_dat)
panda_dat.to_csv(model_filename_no_extension+".paper_images_results.table",mode='w') 
