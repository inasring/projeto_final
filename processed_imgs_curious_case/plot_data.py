#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
np.random.seed(1234)
import sys
import csv
from itertools import izip
from matplotlib import pyplot as plt
import numpy as np
import time
import pickle
import sys
import pylab as pl
import matplotlib as mpl
import scipy as sp
import math
from mpl_toolkits.axes_grid1 import make_axes_locatable, axes_size

#python denoise_gray_cifar10.h5 lena_gray.gif
try:
    numpy_filename = str(sys.argv[1])
except IndexError:
    print("No numpy savez file specified in argv")
    raise


data = np.load(numpy_filename)

img_noisy  = data["img_noisy"]
img_processed  = data["img_processed"]
error_img  = data["error_img"]

print("Loading numpy "+numpy_filename +" ...")

n_cols = 3
nlines_plot = 1
cmap = "gray"

f, axarr = plt.subplots(nlines_plot, n_cols,figsize=(10, 5))
print("image plot axis shape=",axarr.shape)

min_pixel_value = 0
max_pixel_value = 255

axarr[0].set_title(u'Imagem com ruído')
axarr[0].imshow(img_noisy,interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)

axarr[1].set_title(u'Imagem processada')
axarr[1].imshow(img_processed,interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)
#axarr[1].set_xlabel(label % (ssim_denoised, rmse_denoised))

axarr[2].set_title(u'Erro absoluto')
print ("Maximum error = ", np.amax(error_img))
im_proc = axarr[2].imshow(error_img,interpolation='none',cmap='gray_r',vmin=min_pixel_value, vmax=60)
for axis in axarr:
  axis.get_yaxis().set_visible(False)
  axis.get_xaxis().set_visible(False)




box = axarr[2].get_position()
#axColor = plt.axes([box.x0*1.03 + box.width * 1.03, box.y0+0.16, 0.015, 0.45])
axColor = plt.axes([(box.x0 + box.width ) * 1.01, box.y0+0.158, 0.015, box.height*0.59])
f.colorbar(im_proc, cax = axColor, orientation="vertical")






def remove_last(string,separator):
  return separator.join(string.split(separator)[:-1])

name = numpy_filename.split("/")[-1]
name = remove_last(name,".")


f.savefig("gen_imgs/"+name ,bbox_inches='tight')
#fig2.savefig("lena_tests/lena_modelD_bicubicinterp_"+str(dpi)+".png",bbox_inches='tight', dpi = dpi)


#plt.show()




