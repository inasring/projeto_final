#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
np.random.seed(1234)


from keras.utils import np_utils
import keras.backend as K
from sklearn.model_selection import train_test_split
import sys
from keras.preprocessing.image import array_to_img,img_to_array, load_img
from matplotlib import pyplot as plt
import numpy as np
from tqdm import tqdm
from scipy.ndimage.filters import gaussian_filter
from my_utils import *
import pylab as pl
import matplotlib as mpl
import scipy as sp
try:
    image_filename = str(sys.argv[1])
    blur_sigma = str(sys.argv[2])
except IndexError:
    print("No blur sigma or image file specified in argv")
    raise
def remove_extension(filename):
    l = filename.split(".")
    if l[-1]:
        del l[-1]
    return ".".join(l)
print("Blurring image with sigma: ",str(blur_sigma))
sigma  = float(blur_sigma)
print("Loading image...")
img_array      = img_to_array(sp.ndimage.imread(image_filename,mode='RGB'))
img_batch = np.zeros(tuple([1]+[i for i in img_array.shape]))
img_batch[0] = np.copy(img_array)
name = remove_extension(image_filename)
print("Applying gaussian filter...")
blurred_img    = blurr_batch(np.copy(img_batch),sigma=sigma)
print (blurred_img.shape)
sp.misc.imsave(name+str("_blurred_sigma_")+str(blur_sigma)+".png",blurred_img[0].transpose(1,2,0))
