#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.font_manager import FontProperties


from sklearn.model_selection import train_test_split

from keras.models import Model
from keras.layers import Input
from keras.layers.core import Dense,Activation
from keras.optimizers import SGD
from itertools import izip
import csv

np.random.seed(1234)


#random data gen
sup_lim = 3
inf_lim = -3
n_samp  = 300
x = np.linspace(inf_lim,sup_lim,n_samp)
x = x.reshape(x.shape[0],1)
print x.shape
func = lambda x:1./(1+np.exp(-3*x))
y = func(x)


power = 0.1
#noise = np.random.uniform(-power,power,size=x.shape)
noise = np.random.normal(scale=power,size=x.shape)


y_noisy = func(x)+noise
#y_label = (y>0.5).astype("float32")

X_train, X_test, y_train, y_test = train_test_split(x, y_noisy, test_size=0.1)



inp = Input(shape=(1,))
l   = Dense(1)(inp)
out = Activation("sigmoid")(l)
model = Model(input=inp,output=out)

#model.compile(SGD(lr=0.1, momentum=0.9, decay=0.0, nesterov=True), loss='mean_squared_error',
 #             metrics=['mean_squared_error'])

model.compile(SGD(lr=0.1, momentum=0.9, decay=0.0, nesterov=False), loss='mean_squared_error',
              metrics=['mean_squared_error'])

batch_size = 8
nb_epoch   = 10
history=model.fit(X_train, y_train,
          batch_size=batch_size,
          nb_epoch=nb_epoch,
          validation_data=(X_test, y_test),
          shuffle=True)

print model.layers[1].get_weights()

predicted_output = model.predict(X_test).squeeze()
print "predicted_output.shape = ",predicted_output.shape

X_test_squeezed = X_test.squeeze()
print "X_test_squeezed.shape = ",X_test_squeezed.shape


nlines_plot = 1
n_cols_plot = 2


matplotlib.rc('font', family='DejaVu Sans')
prop = FontProperties(fname='/usr/share/fonts/opentype/stix/STIXGeneral-Bold.otf',size='large')

f1=plt.figure(1)
plt.plot(x,y,color="blue",label=u"y")
plt.scatter(x,y_noisy,color="green",label=u"ŷ")
plt.title(u'Saída real y e saída com ruído ŷ', fontproperties=prop)
plt.legend(loc=2)

f2= plt.figure(2)
plt.title(u'', fontproperties=prop)
plt.scatter(X_test,predicted_output,color="red",label=u"Predição")
plt.plot(x,y,color="blue",label=u"y")
plt.legend(loc=2)


loss    = history.history['loss']
val_loss= history.history['val_loss']
f3= plt.figure(3)
plt.title(u'', fontproperties=prop)
epoch_numbers = range(1,len(loss)+1)
plt.plot(epoch_numbers,loss,color="blue",label=u"custo de treino")
plt.plot(epoch_numbers,val_loss,color="green",label=u"custo de validação")
plt.xlabel('epoch')
plt.legend(loc=2)

plt.grid()
plt.show()


from time import gmtime, strftime
results = "regression_example_"+strftime("%Y-%m-%d--%H:%M:%S", gmtime())
f= open(results, 'wb')
writer = csv.writer(f)
writer.writerows(izip(history.history['loss'],history.history['val_loss']))
f.close()