#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
np.random.seed(1234)
from keras.datasets import cifar10
from keras.layers import merge, Input, Dropout
from keras.layers.convolutional import Convolution2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers.pooling import MaxPooling2D,GlobalAveragePooling2D
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.regularizers import l2, activity_l2, l1
from keras.optimizers import SGD, Adam, RMSprop, Nadam
from keras.callbacks import LearningRateScheduler, ModelCheckpoint, EarlyStopping
from keras.callbacks import History
import keras.backend as K
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import sys
import csv
from itertools import izip
from dense_net import dense_net
from keras.preprocessing.image import array_to_img,img_to_array, load_img
from matplotlib import pyplot as plt
import numpy as np
import time
from tqdm import tqdm
from scipy.ndimage.filters import gaussian_filter
from my_utils import *
from keras.models import load_model
import pickle
import sys
import pylab as pl
import matplotlib as mpl
import scipy as sp
import skimage.measure as skm
import math

#python denoise_gray_cifar10.h5 lena_gray.gif
try:
    model_filename = str(sys.argv[1])
    image_filename = str(sys.argv[2])
except IndexError:
    print("No Model or image file specified in argv")
    raise
sigma  = 2.5
print("Loading model...")
denoise_network = load_model(model_filename)


mean = 0.
stddev= math.sqrt(400)

print("Loading image...")
img_array      = img_to_array(sp.ndimage.imread(image_filename,mode='RGB'))
if len(img_array.shape)==3:
    #color image
    img_array = rgb2gray(img_array)
if len(img_array.shape)==2:
    pass
else:
    print("Image size unknowns")
    exit(1)

img_batch = np.zeros(tuple([1,1]+[i for i in img_array.shape]))
img_batch[0] = np.copy(img_array)

print("Applying noise filter...")

#noisy_batch    = noisy_batch(np.copy(img_batch),sigma=sigma)
print("img_array = ",img_array.shape)
noisy_batch = add_gaussian_noise_batch(np.copy(img_batch),mean=mean,stddev=stddev,min_pixel_value=0,max_pixel_value=255.,verbose=True)
##Processing block    [preprocessing]->[network]->[recovering from preprocessing]
print("Preprocessing...")
noisy_batch_preproc = pre_proc_gan(np.copy(noisy_batch))
print("Processing...")
predicted_preproc = denoise_network.predict(np.copy(noisy_batch_preproc),batch_size=64)
#for i in xrange(2):
 #   predicted_preproc = denoise_network.predict(np.copy(predicted_preproc),batch_size=64)

print("Undoing Preprocessing...")
predicted = undo_pre_proc_gan(np.copy(predicted_preproc))






#predicted   /=255.
#img_batch   /=255.
#noisy_batch /=255.


min_pixel_value = 0
max_pixel_value = 255.



n_cols = 3
nlines_plot = 2
cmap = "gray"
n_imgs= len(img_batch)
label = 'MSE: %2.f, SSIM: %.2f, PSNR: %.2f'

img_index = 0
img_original = np.clip(img_batch[img_index],min_pixel_value , max_pixel_value)
img_noisy  = np.clip(noisy_batch[img_index],min_pixel_value , max_pixel_value)
img_processed= np.clip(predicted[img_index],min_pixel_value , max_pixel_value)

# img_original = img_batch[img_index]
# img_noisy  = noisy_batch[img_index]
# img_processed= predicted[img_index]

mse_noisy = mse(img_original.squeeze(), img_noisy.squeeze())
ssim_noisy = skm.compare_ssim(img_original.squeeze(), img_noisy.squeeze(),
              dynamic_range=255.)
psnr_noisy = skm.compare_psnr(img_original.squeeze(), img_noisy.squeeze(),
              dynamic_range=255.)

mse_denoised = mse(img_original.squeeze(), img_processed.squeeze())
ssim_denoised = skm.compare_ssim(img_original.squeeze().astype('float32'), img_processed.squeeze().astype('float32'),
              dynamic_range=255.)
psnr_denoised = skm.compare_psnr(img_original.squeeze().astype('float32'), img_processed.squeeze().astype('float32'),
              dynamic_range=255.)




print("img_original shape = ",img_original.shape)
print("img_noisy shape = ",img_noisy.shape)
print("img_processed shape = ",img_processed.shape)
#print("Loss = ", lossesTestData[img_index])
f, axarr = plt.subplots(nlines_plot, n_cols,figsize=(23, 10))
print("image plot axis shape=",axarr.shape)
axarr[0][0].set_title('Imagem original')
axarr[0][0].imshow(img_original.squeeze(),interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)

axarr[0][1].set_title(u'Imagem com ruído')
axarr[0][1].imshow(img_noisy.squeeze(),interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)
axarr[0][1].set_xlabel(label % (mse_noisy, ssim_noisy,psnr_noisy))

axarr[0][2].set_title(u'Erro absoluto sem processamento')
im_noproc = axarr[0][2].imshow(np.absolute(img_noisy-img_original).squeeze(),interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)
#print(axarr.ravel().tolist())
cbar_original = f.colorbar(im_noproc, ax=axarr[0].ravel().tolist())

axarr[1][0].set_title('Imagem original')
axarr[1][0].imshow(img_original.squeeze(),interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)

axarr[1][1].set_title('Imagem processada')
axarr[1][1].imshow(img_processed.squeeze(),interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)
axarr[1][1].set_xlabel(label % (mse_denoised, ssim_denoised,psnr_denoised))

axarr[1][2].set_title(u'Erro absoluto com processamento')
im_proc = axarr[1][2].imshow(np.absolute(img_processed-img_original).squeeze(),interpolation='none',cmap=cmap,vmin=min_pixel_value, vmax=max_pixel_value)
#print(axarr.ravel().tolist())
cbar_processed = f.colorbar(im_proc, ax=axarr[1].ravel().tolist())


#cbar.ax.set_yticklabels([str(vmin), '0', str(vmax)])  # vertically oriented colorbar
plt.show()



