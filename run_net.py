from __future__ import print_function
import numpy as np
np.random.seed(1234)

from keras.datasets import cifar10
from keras.layers import merge, Input, Dropout
from keras.layers.convolutional import Convolution2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers.pooling import MaxPooling2D,GlobalAveragePooling2D

from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.regularizers import l2, activity_l2, l1
from keras.optimizers import SGD, Adam, RMSprop, Nadam
from keras.callbacks import LearningRateScheduler, ModelCheckpoint, EarlyStopping
from keras.objectives import cosine_proximity
from keras.callbacks import History
import keras.backend as K
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import sys
import csv
from itertools import izip
from dense_net import *
import pickle
from my_utils import *
from tqdm import tqdm
import pylab as pl
from matplotlib import pyplot as plt
import time
sys.setrecursionlimit(10000)
#Usage
# python run_net database.p filename
try:
    dataset_filename = str(sys.argv[1])
    filename = str(sys.argv[2])
except IndexError:
    print("Dataset or Filename not specified in argv")
    raise



img_rows, img_cols = 32, 32
img_channels = 3
nb_classes = 10
penalization = 0.0001

# the data, shuffled and split between train and test sets
noisy_batch = pickle.load(open( str(dataset_filename), "rb" ))
(original_images, _), (_, _) = cifar10.load_data()
#Blurred images are X_train and X_test
#Original images are Y_train and Y_test
X_train, X_test, Y_train, Y_test = train_test_split(noisy_batch,original_images, test_size=0.1,random_state=10)


X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
Y_train = Y_train.astype('float32')
Y_test = Y_test.astype('float32')


X_train, X_test, Y_train, Y_test = pre_proc(X_train,X_test,Y_train,Y_test)





print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')



input_shape = X_train[0].shape
droprate=0.3
batch_size = 64
nb_epoch = 50
data_augmentation = False
n_dense_blocks = 3  

k = 12 #growth rate: number of feature maps at each convolution output inside a dense block
nb_layers = 12



feature_map_n_list = [k]*nb_layers
bnL2norm=0.0001

n_filter_initial = 16
inp = Input(shape=(3,None,None))
#x = Convolution2D(n_filter_initial, 3, 3, border_mode='same', init='he_normal', W_regularizer = l2(bnL2norm),bias=False,input_shape=(1, None, None))(inp)
x,n_filter = denseBlock_layout(inp,feature_map_n_list,n_filter_initial,droprate=droprate)
x = bnReluConvDrop(3, 3, 3,droprate=0,stride=(1,1),weight_decay=1e-4,bnL2norm=0.0001)(x)
x      = merge([x,inp] , mode='sum',concat_axis=1)

reconstructed = x
model = Model(input=inp, output=reconstructed)




#paper
'''def lr_schedule(epoch):
    if epoch < 150: rate = 0.1
    elif epoch < 225: rate = 0.01
    elif epoch < 300: rate = 0.001
    else: rate = 0.0001
    print (rate)
    return rate'''

model.summary()
#exit(1)
#model.compile(SGD(lr=0.1, momentum=0.9, decay=0.0, nesterov=True), loss='mean_squared_error', metrics=['accuracy'])
model.compile(Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004), loss='mean_squared_error',
              metrics=['mean_squared_error'])


#lrate = LearningRateScheduler(lr_schedule)


#filename  = "unblur_cifar10"
filepath  = filename+".h5"
results   = filename+".results"
model_checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='auto')
early_stopping   = EarlyStopping(monitor='val_loss', patience=20, verbose=1, mode='auto')
#callbacks = [lrate,model_checkpoint,early_stopping]
#callbacks = [lrate,model_checkpoint]
callbacks = [model_checkpoint,early_stopping]

print('Not using data augmentation.')
history=model.fit(X_train, Y_train,
            batch_size=batch_size,
            nb_epoch=nb_epoch,
            validation_data=(X_test, Y_test),
            shuffle=True,
            callbacks=callbacks)
f= open(results, 'wb')
writer = csv.writer(f)
writer.writerows(izip(history.history['loss'],history.history['val_loss']))
f.close()
