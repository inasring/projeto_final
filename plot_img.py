#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
np.random.seed(1234)
import pickle
import sys
import scipy as sp
import scipy.ndimage
from matplotlib import pyplot as plt

#python denoise_gray_cifar10.h5 lena_gray.gif
try:
    image_filename = str(sys.argv[1])
except IndexError:
    print("No Image file specified in argv")
    raise
print("Loading image...")
img = sp.ndimage.imread(image_filename,mode='RGB')

cmap = 'gray'
dpi=100
#fig1 = plt.figure(2,figsize = (2,2))
fig1 = plt.figure(1)
ax1 = fig1.add_subplot(111)
print (img.squeeze().shape)
ax1.imshow(img.squeeze(),interpolation='none',cmap=cmap,vmin=0, vmax=255.)


plt.show()



