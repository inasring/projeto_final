from __future__ import print_function
import numpy as np
np.random.seed(1234)

from keras.datasets import cifar10
from keras.layers import merge, Input, Dropout
from keras.layers.convolutional import Convolution2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers.pooling import MaxPooling2D,GlobalAveragePooling2D

from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.regularizers import l2, activity_l2, l1
from keras.optimizers import SGD, Adam, RMSprop, Nadam
from keras.callbacks import LearningRateScheduler, ModelCheckpoint, EarlyStopping
from keras.preprocessing.image import array_to_img,img_to_array, load_img
from keras.objectives import cosine_proximity
from keras.callbacks import History
import keras.backend as K
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import sys
import csv
from itertools import izip
from dense_net import *
import pickle
from my_utils import *
from tqdm import tqdm
import pylab as pl
from matplotlib import pyplot as plt
import time
import theano.tensor as T
from numpy.random import RandomState
from keras.layers.advanced_activations import LeakyReLU
import math
import scipy as sp

sys.setrecursionlimit(10000)
#Usage
# python run_net database.p filename reference_image.gif
# python run_gan.py gaussian_noise_cifar10_mean_0.0_stddev_20.0randSeed_0.p cifar10gan_09122016 images/lena_color.gif
using_reference_image=False
reference_image_filename = ''
try:
    dataset_filename = str(sys.argv[1])
    filename = str(sys.argv[2])
    if len(sys.argv)>=4:
        reference_image_filename = str(sys.argv[3])
        using_reference_image    = True
except IndexError:
    print("Dataset or Filename not specified in argv")
    raise

def negative_binary_crossentropy(output, target, from_logits=False):
        return -T.nnet.binary_crossentropy(output, target)
def negative_categorical_crossentropy(output, target, from_logits=False):
        return -K.categorical_crossentropy(output, target)

img_rows, img_cols = 32, 32
img_channels = 3
nb_classes = 10
penalization = 0.0001


if  using_reference_image:
    mean = 0.
    stddev= math.sqrt(400)
    img_array      = img_to_array(sp.ndimage.imread(reference_image_filename,mode='RGB'))
    img_batch = np.zeros(tuple([1]+[i for i in img_array.shape]))
    img_batch[0] = np.copy(img_array)
    print("Applying noise filter...")
    noisy_reference_image = add_gaussian_noise_batch(np.copy(img_batch),mean=mean,stddev=stddev,min_pixel_value=0,max_pixel_value=255.)


# the data, shuffled and split between train and test sets
noisy_batch = pickle.load(open( str(dataset_filename), "rb" ))
(original_images, _), (_, _) = cifar10.load_data()
n_dataset = 2000
original_images = original_images[0:n_dataset]
noisy_batch     = noisy_batch [ 0 : n_dataset]
#Blurred images are X_train and X_test
#Original images are Y_train and Y_test
X_train, X_test, Y_train, Y_test = train_test_split(noisy_batch,original_images, test_size=0.1,random_state=10)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
Y_train = Y_train.astype('float32')
Y_test = Y_test.astype('float32')


X_train  = pre_proc_gan(X_train)
X_test   = pre_proc_gan(X_test)
Y_train  = pre_proc_gan(Y_train)
Y_test   = pre_proc_gan(Y_test)






state = 0
prng = RandomState(state)
def discriminative_dataset_gen_2(gen_model,noisy_input,real_input):
    global prng
    generated_sample = gen_model.predict(np.copy(noisy_input))
    doubled_shape = tuple([noisy_input.shape[0]*2]+[i for i in noisy_input.shape[1:]])
    dataset_1 = np.empty(doubled_shape)
    dataset_2 = np.empty(doubled_shape)
    Y         = np.empty(doubled_shape[0])

    dataset_1[0:len(dataset_1)/2] = np.copy (real_input)
    dataset_1[ len(dataset_1)/2:] = np.copy (generated_sample)
    
    dataset_2[0:len(dataset_2)/2] = np.copy (noisy_input)
    dataset_2[ len(dataset_2)/2:] = np.copy (noisy_input) 
    
    Y[0:len(Y)/2] = np.ones (len(Y)/2)
    Y[len(Y)/2:]  = np.zeros  (len(Y)/2)

    #shuffle the three arrays the same way
    rng_state = prng.get_state()
    prng.shuffle(dataset_1)
    prng.set_state(rng_state)
    prng.shuffle(dataset_2)
    prng.set_state(rng_state)
    prng.shuffle(Y)


    Y =np.expand_dims(Y,axis=0).T

    return dataset_1,dataset_2,Y




print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')



input_shape = X_train[0].shape
droprate=0.3
batch_size = 64

data_augmentation = False
n_dense_blocks = 3  

k = 12 #growth rate: number of feature maps at each convolution output inside a dense block
nb_layers = 12



feature_map_n_list = [k]*nb_layers
bnL2norm=0.0001
n_filter_initial = 16

#####Generative
inp = Input(shape=(3,None,None))
x = Convolution2D(n_filter_initial, 3, 3, border_mode='same', init='he_normal', W_regularizer = l2(bnL2norm),bias=False,input_shape=(None, None, None))(inp)
x,n_filter = denseBlock_layout_2(x,feature_map_n_list,n_filter_initial,droprate=droprate)
x = bnLeakyReLUConvDrop_2(3, 3,  3,droprate=0,stride=(1,1),weight_decay=1e-4,bnL2norm=0.0001)(x)
x      = merge([x,inp] , mode='sum',concat_axis=1)
x      = Activation('tanh')(x)
reconstructed = x

generative = Model(input=inp, output=reconstructed)
generative.compile(Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004), loss='mean_squared_error',
              metrics=['mean_squared_error'])
generative.summary()
####

####Discriminative
disc_inp_1 = Input(shape=(3,32,32))
disc_inp_2 = Input(shape=(3,32,32))
disc_inp = merge([disc_inp_1, disc_inp_2], mode='concat',concat_axis=1)

#x_1 = Convolution2D(n_filter_initial, 3, 3, border_mode='same', init='he_normal', W_regularizer = l2(bnL2norm),bias=False,input_shape=(None, None, None))(disc_inp_1)
#x_1,n_filter = denseBlock_layout_2(x_1,feature_map_n_list,n_filter_initial,droprate=droprate)
#x_1 = bnReluConvDrop_2(3, 3, 3,droprate=0,stride=(1,1),weight_decay=1e-4,bnL2norm=0.0001)(x_1)
#melhorar modelo -> indo de 3072 para dense 2

x = Convolution2D(n_filter_initial, 3, 3, border_mode='same', init='he_normal', W_regularizer = l2(bnL2norm),bias=False,input_shape=(None, None, None))(disc_inp)
x,n_filter = denseBlock_layout_2_Leaky(x,feature_map_n_list,n_filter_initial,droprate=droprate)
x = bnLeakyReLUConvDrop_2(3, 3, 3,droprate=0,stride=(1,1),weight_decay=1e-4,bnL2norm=0.0001)(x)
x = Flatten()(x)
#x = GlobalAveragePooling2D()(x)
x = Dense(1, activation='sigmoid')(x)
discriminative_output = x
discriminative = Model(input=[disc_inp_1,disc_inp_2], output=discriminative_output)
discriminative.compile(Nadam(lr=0.0002, beta_1=0.5, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004), loss='binary_crossentropy',
              metrics=['accuracy'])
discriminative.summary()
####

####GAN
gan_input_generator = Input(shape=(3,32,32))
real_input_image    = Input(shape=(3,32,32))
generated = generative(gan_input_generator)
discriminative.trainable=False
#Alterar lados somente na hora de treinar a discriminative
discriminated = discriminative([generated,real_input_image])
gan_output = discriminated
GAN    = Model(input=[gan_input_generator,real_input_image],output=gan_output)
GAN.compile(Nadam(lr=0.0002, beta_1=0.5, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004), loss='binary_crossentropy',
            metrics=['accuracy'])
GAN.summary()
####







#GAN labels swapped to 1 so that minimising the error means fooling D
gan_Y = np.ones((len(X_train),1))
gan_Y_test = np.ones((len(X_test),1))

disc_filename=filename+"_gan_discriminative"
filepath_disc  = disc_filename+".h5"
results_disc   = disc_filename+".results"
model_checkpoint_disc = ModelCheckpoint(filepath_disc, monitor='val_acc', verbose=1, save_best_only=True, save_weights_only=False, mode='auto')
#early_stopping_disc   = EarlyStopping(monitor='val_loss', patience=2, verbose=1, mode='auto')
nb_epoch_discriminative = 1
callbacks_discriminative = [model_checkpoint_disc]#,early_stopping_disc]

GAN_filename=filename+"_gan_GAN"
filepath_GAN  = GAN_filename+".h5"
results_GAN   = filename+".results"
#minimum valacc means better fooling
model_checkpoint_gan = ModelCheckpoint(filepath_GAN, monitor='val_acc', verbose=1, save_best_only=True, save_weights_only=False, mode='min')
nb_epoch_gan = 1
early_stopping_gan   = EarlyStopping(monitor='val_loss', patience=2, verbose=1, mode='min')
callbacks_GAN = [model_checkpoint_gan]#,early_stopping_gan]

generative_filename=filename+"_gan_generative"
filepath_generative  = generative_filename+".h5"
results_generative =generative_filename+".results"




discriminative_X_train_1,discriminative_X_train_2,discriminative_Y_train = discriminative_dataset_gen_2(generative,X_train,Y_train)
discriminative_X_test_1,discriminative_X_test_2,discriminative_Y_test = discriminative_dataset_gen_2(generative,X_test,Y_test)

mse_error_old = float("inf")
try:
    disc_history = []
    GAN_history  = []
    n_combined_epochs = 30
    for combined_train_index in xrange(n_combined_epochs):
        print ("Combined training epoch = ", combined_train_index, " of a total of ", n_combined_epochs)
        

        print("Training Discriminative network -> Accuracy should increase")
        disc_h=discriminative.fit([discriminative_X_train_1,discriminative_X_train_2], discriminative_Y_train,
                            batch_size=batch_size,
                            nb_epoch=nb_epoch_discriminative,
                            validation_data=([discriminative_X_test_1,discriminative_X_test_2], discriminative_Y_test),
                            shuffle=True,
                            callbacks=callbacks_discriminative)




        print("Training GAN  -> Accuracy should decrease")
        gan_h=GAN.fit([X_train,Y_train], gan_Y,
                            batch_size=batch_size,
                            nb_epoch=nb_epoch_gan,
                            validation_data=([X_test,Y_test], gan_Y_test),
                            shuffle=True,
                            callbacks=callbacks_GAN)
        print("Generating new dataset...")
        discriminative_X_train_1,discriminative_X_train_2,discriminative_Y_train = discriminative_dataset_gen_2(generative,X_train,Y_train)
        discriminative_X_test_1,discriminative_X_test_2,discriminative_Y_test = discriminative_dataset_gen_2(generative,X_test,Y_test)
        disc_history.append(disc_h)
        GAN_history.append(gan_h)      

        if  using_reference_image:
            ##Processing block    [preprocessing]->[network]->[recovering from preprocessing]
            print("Preprocessing reference image...")
            noisy_reference_image_preproc = pre_proc_gan(np.copy(noisy_reference_image))
            print("Processing reference image...")
            predicted_preproc = generative.predict(np.copy(noisy_reference_image_preproc),batch_size=64)
            print("Undoing Preprocessing on reference image...")
            predicted = undo_pre_proc_gan(np.copy(predicted_preproc))
            mse_error =  mse(img_batch[0].transpose(1, 2, 0)/255.,predicted[0].transpose(1, 2, 0)/255.)
            if mse_error<mse_error_old:
                print("MSE  reduced from ",mse_error_old," to ",mse_error)
                print("Saving generative model to ",filepath_generative)
                generative.save(filepath_generative)
                mse_error_old = mse_error
            else:
                print ("current MSE = ",mse_error,"current best MSE = ", mse_error_old)
        else:
                print("Saving generative model to ",filepath_generative)
                generative.save(filepath_generative)
        
      

except KeyboardInterrupt:
    pass



print("Saving generative model and results...")
generative.save(filepath_generative)

f= open(results_disc, 'wb')
writer = csv.writer(f)
loss = [i for i in disc_h.history['loss']]
val_loss = [i for i in disc_h.history['val_loss']]
acc  = [i for i in disc_h.history['acc']]
val_acc  = [i for i in disc_h.history['val_acc']]
writer.writerows(izip(loss,val_loss,acc,val_acc))
f.close()

f= open(results_GAN, 'wb')
writer = csv.writer(f)
loss = [i for i in gan_h.history['loss']]
val_loss = [i for i in gan_h.history['val_loss']]
acc  = [i for i in gan_h.history['acc']]
val_acc  = [i for i in gan_h.history['val_acc']]
writer.writerows(izip(loss,val_loss,acc,val_acc))
f.close()