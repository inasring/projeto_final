from __future__ import print_function
import numpy as np
np.random.seed(1234)

from keras.datasets import mnist, cifar10
from keras.models import Sequential
from keras.callbacks import History 
from keras.layers import Input, Reshape
from keras.layers.convolutional import UpSampling2D
from keras.layers.core import Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.models import Model
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.normalization import BatchNormalization
from keras.optimizers import SGD, Adam, RMSprop, Nadam
from keras.layers.advanced_activations import PReLU
from keras.utils import np_utils
from dense_net import *
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys

sys.setrecursionlimit(10000)

nNoise = 100

LR = 0.001
batch_size = 32
nb_epoch = 1000

(X_train, y_train), (X_test, y_test) = mnist.load_data()

img_rows = img_cols = 28
nb_classes = 10
nPoints = X_train.shape[0]

X_train = X_train.reshape(X_train.shape[0], 1, img_rows, img_cols)
X_test = X_test.reshape(X_test.shape[0], 1, img_rows, img_cols)

Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train -= 127.5
X_test -= 127.5
X_train /= 127.5
X_test /= 127.5

np.random.shuffle(X_train)

nChannels = 128

def gen(input):
    k = 12 #growth rate: number of feature maps at each convolution output inside a dense block
    nb_layers_g = 9
    n_filter = 0
    feature_map_n_list_g = [k]*nb_layers_g
    x = Reshape((nChannels,img_rows,img_cols))(input)
    x,n_filter = denseBlock_layout_2(x,feature_map_n_list_g,n_filter,droprate=0.5)
    x = bnLeakyReLUConvDrop_2(1, 3,  3,droprate=droprate_g,stride=(1,1),weight_decay=1e-4,bnL2norm=0.0001)(x)
    net = Activation('tanh')(x)
    return net

gInput = Input(shape=(nNoise,))
gModel = Model(input=gInput, output=gen(gInput))
gModel.compile(loss='binary_crossentropy', optimizer=Adam(lr=LR), metrics=['accuracy'])

gModel.summary()

def discrim(input):
    net = Convolution2D(128,3,3,subsample=(2,2),border_mode='same')(input)
    net = Activation('relu')(net)
    net = Convolution2D(256,3,3,subsample=(2,2),border_mode='same')(net)
    net = Activation('relu')(net)
    net = Flatten()(net)
    net = Dense(256)(net)
    net = Activation('relu')(net)
    net = Dense(1)(net)
    net = Activation('sigmoid')(net)
    return net

dInput = Input(shape=(1,img_rows,img_cols,))
dModel = Model(input=dInput, output=discrim(dInput))
dModel.compile(loss='binary_crossentropy', optimizer=SGD(lr=LR*10.0), metrics=['accuracy'])

dModel.summary()

def make_trainable(net, val):
    net.trainable = val
    for l in net.layers: l.trainable = val


make_trainable(dModel, False)

gan_input = Input(shape=(nNoise,))
gan_V = dModel(gModel(gan_input))
GAN = Model(gan_input, gan_V)
GAN.compile(loss='binary_crossentropy', optimizer=Adam(lr=LR), metrics=['accuracy'])

realPoints = X_train

for i in range(nb_epoch):
    noise = np.random.uniform(0,1,size=[batch_size*2,nNoise])
    gLoss, gAcc = GAN.train_on_batch(noise, [1]*batch_size*2)
    print("--- GENERATOR ---\nLoss: %s      Acc: %s"%(gLoss,gAcc))

    if gAcc < 0.3: continue

    noise = np.random.uniform(0,1,size=[batch_size,nNoise])
    fakePoints = gModel.predict(noise)

    realPointsBatch = realPoints[np.random.randint(0,nPoints,size=batch_size)]

    packedPoints = np.concatenate((realPointsBatch, fakePoints))
    labels = np.zeros(2*batch_size)
    labels[:batch_size] = 1

    dLoss, dAcc = dModel.train_on_batch(packedPoints, labels)
    print("--- DISCRIMINATOR ---\nLoss: %s      Acc: %s"%(dLoss,dAcc))

noise = np.random.uniform(0,1,size=[16,nNoise])
fakePoints = gModel.predict(noise)

plt.figure(figsize=(4,4))
for i in range(16):
    plt.subplot(4,4,i+1)
    plt.imshow(fakePoints[i,0,:,:], cmap='Greys')
    plt.axis('off')
plt.tight_layout()
plt.show()
plt.savefig('gen.png')



#f = open("results","a")
#f.write("\n".join(map(lambda x: str(x), history.history['val_acc'])))
#f.close()

#g = open("trainResults","a")
#g.write("\n".join(map(lambda x: str(x), history.history['acc'])))
#g.close()
